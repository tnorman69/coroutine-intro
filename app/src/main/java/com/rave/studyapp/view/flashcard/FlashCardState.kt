package com.rave.studyapp.view.flashcard

data class FlashCardState(
    val topic: String = "",
    val lifeCycleDesc: String = "",
    val enablePrev: Boolean = false,
    val enableNext: Boolean = false,
    val isLoading: Boolean = true,
)
