package com.rave.studyapp.view.flashcard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rave.studyapp.databinding.FragmentFlashCardBinding
import com.rave.studyapp.viewmodel.FlashCardViewModel

class FlashCardFragment : Fragment() {

    private var _binding: FragmentFlashCardBinding? = null
    private val binding get() = _binding!!

    private val flashCardVM by viewModels<FlashCardViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFlashCardBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        flashCardVM.state.observe(viewLifecycleOwner) { flashCardState ->
            handleState(flashCardState)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() = with(binding) {
        btnClose.setOnClickListener { findNavController().navigateUp() }
        btnPrev.setOnClickListener { flashCardVM.prevTopic() }
        btnNext.setOnClickListener { flashCardVM.nextTopic() }
    }

    private fun handleState(state: FlashCardState) = with(binding) {
        tvTopic.text = state.topic
        tvLifeCycleDesc.text= state.lifeCycleDesc
        btnNext.isEnabled = state.enableNext
        btnPrev.isEnabled = state.enablePrev
        progress.isVisible = state.isLoading
    }
}