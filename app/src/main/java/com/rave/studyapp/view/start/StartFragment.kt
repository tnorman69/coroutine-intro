package com.rave.studyapp.view.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.studyapp.databinding.FragmentStartBinding
import androidx.fragment.app.viewModels
import com.rave.studyapp.viewmodel.FlashCardViewModel

class StartFragment : Fragment() {

    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModels<FlashCardViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentStartBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val ghostButton = binding.btnNext
        ghostButton.setVisibility(View.INVISIBLE)
        super.onViewCreated(view, savedInstanceState)
        binding.btnGetColor.setOnClickListener {
            viewModel.randomizeColors()
            ghostButton.setVisibility(View.VISIBLE)
        }

        viewModel.color.observe(viewLifecycleOwner) { color ->
            binding.colorCard.setBackgroundColor(color)
        }

        binding.btnNext.setOnClickListener {
            viewModel.color.value?.let {
                val colorFragmentActions =
                    StartFragmentDirections.goToColorFragment(it)
                findNavController().navigate(colorFragmentActions)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}