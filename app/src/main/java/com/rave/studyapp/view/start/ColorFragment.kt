package com.rave.studyapp.view.start

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.rave.studyapp.databinding.FragmentColorBinding

class ColorFragment : Fragment() {

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<ColorFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.setBackgroundColor(args.color)
        binding.btnColorNext.setOnClickListener {
            val colorDirection = ColorFragmentDirections.goToFlashCardFragment()
            findNavController().navigate(colorDirection)
        }
        binding.btnColorBack.setOnClickListener {
            val colorDirection = ColorFragmentDirections.goToStartFragment()
            findNavController().navigate(colorDirection)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}