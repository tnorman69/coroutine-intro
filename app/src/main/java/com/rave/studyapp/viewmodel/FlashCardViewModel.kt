package com.rave.studyapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.studyapp.model.ColorRepo
import com.rave.studyapp.model.StudyRepo
import com.rave.studyapp.view.flashcard.FlashCardState
import kotlinx.coroutines.launch

class FlashCardViewModel : ViewModel() {

    private val studyRepo = StudyRepo

    private var studyTopics: List<String> = emptyList()
    private var currentTopicIndex: Int = 0
    private var studyDesc: List<String> = emptyList()
    private var currentDescIndex: Int = 0

    private val enableNext get() = studyTopics.size.minus(1) > currentTopicIndex
    private val enablePrev get() = currentTopicIndex > 0

    private val _state = MutableLiveData(FlashCardState())
    val state: LiveData<FlashCardState> get() = _state

    init {
        viewModelScope.launch {
            studyTopics = studyRepo.getStudyTopics()
            studyDesc = studyRepo.getStudyDesc()
            updateState()
        }
    }

    private fun updateState() {
        _state.value = FlashCardState(
            topic = studyTopics[currentTopicIndex],
            lifeCycleDesc = studyDesc[currentDescIndex] ,
            enableNext = enableNext,
            enablePrev = enablePrev,
            isLoading = false
        )
    }

    fun nextTopic() {
        if (enableNext) {
            currentTopicIndex++
            currentDescIndex++
            updateState()
        }
    }

    fun prevTopic() {
        if (enablePrev) {
            currentTopicIndex--
            currentDescIndex--
            updateState()
        }
    }

    private val colorRepo = ColorRepo

    private val _color = MutableLiveData<Int>()
    val color: LiveData<Int> = _color


    fun randomizeColors() {
        _color.value = colorRepo.getColors()
    }
}