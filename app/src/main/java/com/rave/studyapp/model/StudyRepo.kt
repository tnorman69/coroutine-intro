package com.rave.studyapp.model

import com.rave.studyapp.model.remote.ColorApi
import com.rave.studyapp.model.remote.StudyService
import com.rave.studyapp.model.remote.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object StudyRepo {
    private val studyService: StudyService = object : StudyService {
        override suspend fun getStudyTopics() = listOf(
            "onCreate",
            "onStart",
            "onResume",
            "onPause",
            "onStop",
            "onRestart",
            "onDestroy",
            "onAttach",
            "onViewCreated",
            "onViewDestroyed",
            "onDetach",
        )
        override suspend fun getStudyDesc() = listOf(
            "Code ran when the fragment is created.",
            "Code ran when the fragment is initialized and running.",
            "Code ran when the fragment is running after a pause in its active state.",
            "Code ran when the fragment is temporarily halted from running; fragment still exists.",
            "Code ran when the fragment ceases functionality; fragment still exists.",
            "Code ran when the fragment loses and then regains focus.",
            "Code ran when the fragment instance is deleted.",
            "Code ran when the fragment attaches to its activity host.",
            "Code ran when a view within a fragment is made.",
            "Code ran when a view within a fragment instance is deleted.",
            "Code ran when a fragment detaches from its activity host.",
        )
    }

    suspend fun getStudyTopics(): List<String> = withContext(Dispatchers.IO) {
        delay(3000)
        return@withContext studyService.getStudyTopics()
    }
    suspend fun getStudyDesc(): List<String> = withContext(Dispatchers.IO) {
        return@withContext studyService.getStudyDesc()
    }
}

object ColorRepo {

    private val colorApi = object : ColorApi {
        override fun getColors() =  randomColor
    }

    fun getColors() = colorApi.getColors()
}