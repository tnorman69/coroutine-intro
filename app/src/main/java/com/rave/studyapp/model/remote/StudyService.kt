package com.rave.studyapp.model.remote

import android.graphics.Color
import kotlin.random.Random

interface StudyService {

    suspend fun getStudyTopics() : List<String>
    suspend fun getStudyDesc() : List<String>
}

interface ColorApi {
        fun getColors(): Int
    }

val randomColor
    get() = Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))